import cfnresponse
import boto3

def lambda_handler(event, context):

    response_data = {}
    try:
        request_type = event['RequestType']
        emailaddress = event['ResourceProperties']['EmailAddress']

        ses = boto3.client('ses')

        if request_type in ['Create', 'Update']:
            result = ses.list_verified_email_addresses()
            addresses = result['VerifiedEmailAddresses']
            if emailaddress not in addresses:
                ses.verify_email_address(EmailAddress=emailaddress)
            response_data['EmailAddress'] = emailaddress

        elif request_type == 'Delete':
            result = ses.list_verified_email_addresses()
            addresses = result['VerifiedEmailAddresses']
            if emailaddress in addresses:
                ses.delete_verified_email_address(EmailAddress=emailaddress)
            response_data['EmailAddress'] = emailaddress

        cfnresponse.send(event, context, cfnresponse.SUCCESS, response_data, emailaddress)

    except Exception as e:
        print 'Exception occured: ' + str(e)
        cfnresponse.send(event, context, cfnresponse.FAILED, response_data)
        raise e